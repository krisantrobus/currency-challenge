import { Injectable, Pipe, PipeTransform, NgModule } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { AppStateRoot } from "../store/state";
import { getShowCurrency } from "../modules/currencies/store/selector";
import { Observable, Subscription } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class TranslateService {
  constructor(private _store: Store<AppStateRoot>) {}

  private region: Observable<string> = this._store.pipe(
    select(getShowCurrency)
  );
  private regionSub: Subscription;

  public getCurrencySymbolText = () => {
    let extractRegion: string;

    this.regionSub = this.region.subscribe(region => {
      extractRegion = region;
      // this.regionSub.unsubscribe();
    });

    switch (extractRegion.toLowerCase()) {
      case "usd":
        return "$";
      default:
        return "£";
    }
  };
}

@Pipe({
  name: "addCurrencySymbol",
  pure: false
})
export class TranslateAddCurrencySymbol implements PipeTransform {
  constructor(private _store: Store<AppStateRoot>) {}

  private region: Observable<string> = this._store.pipe(
    select(getShowCurrency)
  );

  transform(value: string): any {
    let extractRegion: string;

    this.region
      .subscribe(region => {
        extractRegion = region;
      })
      .unsubscribe();

    switch (extractRegion.toLowerCase()) {
      case "usd":
        return `\$${value}`;
      default:
        return `£${value}`;
    }
  }
}

@NgModule({
  declarations: [TranslateAddCurrencySymbol],
  providers: [TranslateService],
  exports: [TranslateAddCurrencySymbol]
})
export class PipeTransformModule {}
