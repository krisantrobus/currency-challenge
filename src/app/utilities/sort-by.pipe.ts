import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "sortBy"
})
export class SortByPipe implements PipeTransform {
  transform(value: Array<any>, field?: string): any {
    if (value.length > 0) {
      value.sort((a, b) => {
        if (field === void 0) {
          if (a < b) {
            return -1;
          } else if (a > b) {
            return 1;
          }
          return 0;
        } else {
          if (a[field] < b[field]) {
            return -1;
          } else if (a[field] > b[field]) {
            return 1;
          }
          return 0;
        }
      });
    }
    return value;
  }
}
