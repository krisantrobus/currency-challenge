import { Directive, ElementRef, HostListener, Renderer2 } from "@angular/core";
import { setClassMetadata } from "@angular/core/src/render3";

@Directive({
  selector: "[hover]"
})
export class HoverDirective {
  constructor(private el: ElementRef, private renderer: Renderer2) {}

  // run when hovering over the element
  @HostListener("mouseenter") onMouseEnter() {
    this.highlight(true);
  }

  // run when leaving the element
  @HostListener("mouseleave") onMouseLeave() {
    this.highlight(false);
  }

  // Highlighted based on whether the mouse is over the element by applying styling
  private highlight(active: boolean): void {
    active
      ? this.renderer.addClass(this.el.nativeElement, "hover-directive")
      : this.renderer.removeClass(this.el.nativeElement, "hover-directive");
  }
}
