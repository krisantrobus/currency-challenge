import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "filterCurrencyList"
})
export class FilterCurrencyListPipe implements PipeTransform {
  transform(currencies: Object[], searchValue: string): any {
    if (currencies.length > 0 && searchValue) {
      let filteredList = currencies.map((currency: object) => {
        const currencyKey: string = Object.keys(currency)[0];
        if (
          currencyKey &&
          (currencyKey.toLowerCase().includes(searchValue.toLowerCase()) ||
            currency[currencyKey]
              .toLowerCase()
              .includes(searchValue.toLowerCase()))
        ) {
          return currency;
        }
      });
      return filteredList;
    }
    return currencies;
  }
}
