import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { appReducer } from "./store/app.reducer";
import { StoreModule } from "@ngrx/store";
import { TranslateService } from "./utilities/translate.service";
import { LoadingComponent } from "./components/loading/loading.component";

@NgModule({
  declarations: [AppComponent, NavbarComponent, LoadingComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    StoreModule.forRoot({}),
    StoreModule.forFeature("appState", appReducer)
  ],
  providers: [TranslateService],
  bootstrap: [AppComponent]
})
export class AppModule {}
