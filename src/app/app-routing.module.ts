import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    redirectTo: "currencies",
    pathMatch: "full"
  },
  {
    path: "currencies",
    loadChildren: () => import('./modules/currencies/currencies.module').then(m => m.CurrenciesModule)
  },
  {
    path: "documentation",
    loadChildren:
      () => import('./modules/documentation/documentation.module').then(m => m.DocumentationModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
