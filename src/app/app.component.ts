import { Component, OnInit } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { AppStateRoot, AppActions } from "./store/state";
import { Observable } from "rxjs";
import { getLoading } from "./store/selector";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  constructor(private _store: Store<AppStateRoot>) {}

  public loading: Observable<boolean>;

  ngOnInit() {
    // Set scrollable height for mobile browsers as they have headers
    this.setMaxHeight();
    window.addEventListener("resize", this.setMaxHeight);
    this.loading = this._store.pipe(select(getLoading));
  }

  private setMaxHeight = () => {
    document.body.style.height = `${window.innerHeight}px`;
  };
}
