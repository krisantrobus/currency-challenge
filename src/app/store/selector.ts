import { createSelector } from "@ngrx/store";
import { AppState, AppStateRoot } from "src/app/store/state";

const state = (state: AppStateRoot) => state.appState;

export const getLoading = createSelector(
  state,
  (state: AppState) => state.loading
);
