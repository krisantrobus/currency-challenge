import { initialAppState, AppState } from "./state";
import { AppActions, Action } from "./state";

export function appReducer(state: AppState = initialAppState, action: Action) {
  switch (action.type) {
    case AppActions.TOGGLE_LOADING:
      console.log(`runnning update`);

      console.log(state);

      return { ...state, loading: !state.loading };
    default:
      return state;
  }
}
