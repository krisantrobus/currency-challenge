import { CoinState } from "../modules/currencies/store/state";

export interface AppStateRoot {
  coinFeature: CoinState;
  appState: AppState;
}

export interface AppState {
  loading: boolean;
}

export enum AppActions {
  TOGGLE_LOADING = "TOGGLE_LOADING"
}

export interface Action {
  type: String;
  payload: any;
}

export const initialAppState: AppState = {
  loading: false
};
