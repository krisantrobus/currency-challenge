import { CoinSelection } from "./types";

export const COINS: CoinSelection = {
  bitcoin: "btc",
  tether: "usdt",
  bitcoinCash: "bch",
  litecoin: "ltc"
};
