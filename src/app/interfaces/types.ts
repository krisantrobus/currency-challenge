export interface Coin {
  source: string;
  coin_id: string;
  success: boolean;
  currency: string;
  utc_date: Date;
  coin_name: string;
  last_price: string;
  time_stamp: string;
  volume_24hr: string;
  currency_name: string;
  vol_24hr_pcnt: number;
  price_24hr_pcnt: number;
}

export interface CoinSelection {
  bitcoin: string;
  tether: string;
  bitcoinCash: string;
  litecoin: string;
}
