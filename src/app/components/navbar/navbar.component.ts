import { Component, OnInit } from "@angular/core";
import {
  trigger,
  state,
  style,
  transition,
  animate
} from "@angular/animations";
import {
  faHome,
  faBars,
  faFileAlt,
  IconDefinition,
  faSquare
} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"],
  animations: [
    trigger("openClose", [
      state(
        "open",
        style({
          marginLeft: "0"
        })
      ),
      state(
        "closed",
        style({
          marginLeft: "-170px"
        })
      ),
      transition("open => *", [animate("0.3s 200ms ease-out")]),
      transition("closed => open", [animate("0.3s 200ms ease-in")])
    ]),
    trigger("overlay", [
      state(
        "show",
        style({
          opacity: 0.2
        })
      ),
      state(
        "hide",
        style({
          opacity: 0,
          display: "none"
        })
      ),
      transition("show => *", [animate("0.3s 200ms ease-out")]),
      transition("hide => show", [animate("0.3s 200ms ease-in")])
    ])
  ]
})
export class NavbarComponent implements OnInit {
  constructor() {}

  public active: boolean;

  ngOnInit() {
    // Collapse sidebar by default on phones
    this.active = window.innerWidth < 767 ? false : true;
  }

  public faHome: IconDefinition = faHome;
  public faBars: IconDefinition = faBars;
  public faFileAlt: IconDefinition = faFileAlt;
  public faSquare: IconDefinition = faSquare;

  public toggleExpand = () => {
    this.active = !this.active;
  };
}
