import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-loading",
  template: `
    <div
      id="loadingContainer"
      class="h-100 w-100 justify-content-center align-items-center d-flex"
    ></div>
    <div class="spinner-border text-primary" role="status"></div>
  `,
  styleUrls: ["./loading.component.scss"]
})
export class LoadingComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
