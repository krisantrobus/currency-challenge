import { Component, OnInit, OnDestroy } from "@angular/core";
import { CurrencyDataService } from "../../currencies.service";
import { Observable } from "rxjs";
import { Store, select } from "@ngrx/store";
import { AppStateRoot } from "src/app/store/state";
import { Coin } from "src/app/interfaces/types";
import { getStoreCoins } from "../../store/selector";

@Component({
  selector: "app-all-currencies",
  templateUrl: "./all-currencies.component.html",
  styleUrls: ["./all-currencies.component.scss"]
})
export class AllCurrenciesComponent implements OnInit, OnDestroy {
  constructor(
    private cds: CurrencyDataService,
    private _store: Store<AppStateRoot>
  ) {}

  public coins: Observable<Coin[]>;

  ngOnInit() {
    // Uses async pipe to unsubscribe
    this.coins = this._store.pipe(select(getStoreCoins));

    this.getAllCoins();
  }

  ngOnDestroy() {
    this.cds.unsubscribeCoinsToSearch();
  }

  private getAllCoins = async () => {
    let coinsPopulated: boolean = false;
    this.coins.forEach((coins: Coin[]) => {
      coinsPopulated = coins.length > 0;
    });

    !coinsPopulated && this.cds.getAllSpecifiedCurrencies();
  };
}
