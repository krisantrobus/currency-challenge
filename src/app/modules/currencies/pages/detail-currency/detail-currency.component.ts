import { Component, OnInit } from "@angular/core";
import { IconDefinition } from "@fortawesome/fontawesome-svg-core";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { AppStateRoot } from "src/app/store/state";
import { Store, select } from "@ngrx/store";
import { Coin } from "src/app/interfaces/types";
import { Observable } from "rxjs";
import { getSelectedCoin } from "../../store/selector";
import { Location } from "@angular/common";
import { ActivatedRoute } from "@angular/router";
import { CurrencyDataService } from "../../currencies.service";

@Component({
  selector: "app-detail-currency",
  templateUrl: "./detail-currency.component.html",
  styleUrls: ["./detail-currency.component.scss"]
})
export class DetailCurrencyComponent implements OnInit {
  constructor(
    private _store: Store<AppStateRoot>,
    private _location: Location,
    private _activatedRoute: ActivatedRoute,
    private cds: CurrencyDataService
  ) {}

  public faChevronLeft: IconDefinition = faChevronLeft;
  public coin: Observable<Coin>;

  ngOnInit() {
    this.coin = this._store.pipe(select(getSelectedCoin));
    // Only make a call to the api if data is not selected
    // This only happens on refresh of a page
    !this.checkSelectedCoinPopulated() && this.getCoinData();
  }

  public getFormattedPrice = (price: string) => {
    return parseFloat(price).toFixed(2);
  };

  public calculateQuantity = (currentValue: string, purchasePrice: number) => {
    return purchasePrice / parseFloat(currentValue);
  };

  public navigateBack = () => {
    this._location.back();
  };

  private checkSelectedCoinPopulated = () => {
    const coinIdToCheck: string = this.getCoinIdRouterParameter();
    let coinPopulated: boolean = false;
    this.coin.forEach((coin: Coin) => {
      coinPopulated = coin && coin.coin_id == coinIdToCheck ? true : false;
    });
    return coinPopulated;
  };

  private getCoinIdRouterParameter = () => {
    return this._activatedRoute.snapshot.params["coinId"];
  };

  private getCoinData = () => {
    this.cds.getCoinById(this.getCoinIdRouterParameter());
  };
}
