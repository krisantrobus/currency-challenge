import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { CurrenciesRoutingModule } from "./currencies-routing.module";
import { AllCurrenciesComponent } from "./pages/all-currencies/all-currencies.component";
import { DetailCurrencyComponent } from "./pages/detail-currency/detail-currency.component";
import { CurrencyDataService } from "./currencies.service";
import { HttpClientModule } from "@angular/common/http";
import { StoreModule } from "@ngrx/store";
import { coinReducer } from "./store/reducer";
import { CurrencyCardComponent } from "./components/currency-card/currency-card.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { SortByPipe } from "src/app/utilities/sort-by.pipe";
import { PipeTransformModule } from "src/app/utilities/translate.service";
import { HoverModule } from "src/app/utilities/directives/hover.module";
import { CurrencyOptionsComponent } from "./components/currency-options/currency-options.component";
import { AddCurrencyToListItemComponent } from "./components/add-currency-to-list-item/add-currency-to-list-item.component";
import { FilterCurrencyListPipe } from "src/app/utilities/filter-currency-list.pipe";

@NgModule({
  declarations: [
    AllCurrenciesComponent,
    DetailCurrencyComponent,
    CurrencyCardComponent,
    SortByPipe,
    CurrencyOptionsComponent,
    AddCurrencyToListItemComponent,
    FilterCurrencyListPipe
  ],
  imports: [
    CommonModule,
    CurrenciesRoutingModule,
    HttpClientModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forFeature("coinFeature", coinReducer),
    PipeTransformModule,
    HoverModule
  ],
  providers: [CurrencyDataService]
})
export class CurrenciesModule {}
