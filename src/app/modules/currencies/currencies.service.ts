import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Observable, of, Subscription } from "rxjs";
import { environment } from "src/environments/environment";
import { Coin } from "src/app/interfaces/types";
import { COINS } from "src/app/interfaces/const";
import { Store, select } from "@ngrx/store";
import { AppStateRoot, AppActions } from "src/app/store/state";
import { CoinActions } from "./store/action.type";
import {
  getCoinsToSearch,
  getShowCurrency,
  getStoreCoins
} from "./store/selector";

@Injectable()
export class CurrencyDataService {
  constructor(private http: HttpClient, private _store: Store<AppStateRoot>) {}

  private _currencyToShow = "usd";

  private _headers = new HttpHeaders({
    "Content-Type": "application/json",
    "x-rapidapi-host": environment.apiHostHeader,
    "x-rapidapi-key": environment.apiKeyHeader
  });

  private coinsToSearchSubscription: Subscription;
  private currencyToSearch: Subscription;

  public getAllSpecifiedCurrencies = async () => {
    this.currencyToSearch = this._store
      .pipe(select(getShowCurrency))
      .subscribe(currency => {
        this._currencyToShow = currency;
      });

    this.coinsToSearchSubscription = this._store
      .pipe(select(getCoinsToSearch))
      .subscribe(async (coinIds: string[]) => {
        coinIds.forEach(async coinId => {
          const requestSub: Subscription = this.http
            .get<Coin[]>(
              `${environment.apiUrl}/ticker?show=${this._currencyToShow}&coin=${coinId}`,
              {
                headers: this._headers
              }
            )
            .subscribe(
              response => {
                this._store.dispatch({
                  type: CoinActions.UPDATE_COINS,
                  payload: response
                });
                requestSub.unsubscribe();
              },
              err => {
                requestSub.unsubscribe();
                console.log(`for coin: ${coinId}`, err);
              }
            );
        });
      });
  };

  public getCoinById = (coinId: string) => {
    this.currencyToSearch = this._store
      .pipe(select(getShowCurrency))
      .subscribe(currency => {
        this._currencyToShow = currency;
      });

    this._store.dispatch({
      type: AppActions.TOGGLE_LOADING
    });

    const requestSub: Subscription = this.http
      .get<Coin>(
        `${environment.apiUrl}/ticker?show=${this._currencyToShow}&coin=${coinId}`,
        {
          headers: this._headers
        }
      )
      .subscribe(
        response => {
          this._store.dispatch({
            type: CoinActions.SELECT_COIN,
            payload: response
          });
          this._store.dispatch({
            type: AppActions.TOGGLE_LOADING
          });
          requestSub.unsubscribe();
        },
        err => {
          requestSub.unsubscribe();
          console.log(`for coin: ${coinId}`, err);
        }
      );
  };

  public addCoinById = (coinId: string) => {
    console.log(`adding: ${coinId}`);

    this.currencyToSearch = this._store
      .pipe(select(getShowCurrency))
      .subscribe(currency => {
        this._currencyToShow = currency;
      });

    const requestSub: Subscription = this.http
      .get<Coin>(
        `${environment.apiUrl}/ticker?show=${
          this._currencyToShow
        }&coin=${coinId.toLowerCase()}`,
        {
          headers: this._headers
        }
      )
      .subscribe(
        response => {
          this._store.dispatch({
            type: CoinActions.UPDATE_COINS,
            payload: response
          });
          requestSub.unsubscribe();
        },
        err => {
          console.log(`for coin: ${coinId}`, err);
          requestSub.unsubscribe();
        }
      );
  };

  public unsubscribeCoinsToSearch = () => {
    this.coinsToSearchSubscription.unsubscribe();
    this.currencyToSearch.unsubscribe();
  };

  public getAllCurrencySymbols = () => {
    this.http
      .get<Coin[]>(`${environment.apiUrl}/digital-currency-symbols`, {
        headers: this._headers
      })
      .subscribe(
        (response: any) => {
          this._store.dispatch({
            type: CoinActions.UPDATE_CURRENCY_LIST,
            payload: response.digital_currencies
          });
        },
        err => {
          console.log(`error getting all currency symbols`);
        }
      );
  };
}
