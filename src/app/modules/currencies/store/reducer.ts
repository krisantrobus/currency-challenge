import { Action } from "src/app/store/state";
import { CoinActions } from "./action.type";
import { Coin } from "src/app/interfaces/types";
import { CoinState, initialCoinState } from "./state";

export function coinReducer(
  state: CoinState = initialCoinState,
  action: Action
) {
  switch (action.type) {
    case CoinActions.SELECT_COIN:
      return { ...state, selectedCoin: action.payload };
    case CoinActions.UPDATE_COINS:
      return {
        ...state,
        coins: updateCoin(action.payload, state.coins)
      };
    case CoinActions.UPDATE_SHOW_CURRENCY:
      return { ...state, showCurrency: action.payload };
    case CoinActions.UPDATE_CURRENCY_LIST:
      return { ...state, listOfCurrencies: action.payload };
    case CoinActions.UPDATE_COINS_TO_SHOW:
      return {
        ...state,
        coinsToSearch: updateCoinsToShow(action.payload, state.coinsToSearch)
      };
    default: {
      return state;
    }
  }
}

const updateCoin = (coin: Coin, coins: Coin[]) => {
  const coinExists: boolean =
    coins.filter(coinItem => coin.coin_id == coinItem.coin_id).length > 0
      ? true
      : false;

  if (coinExists) {
    return coins.map(coinItem => {
      if (coinItem.coin_id == coin.coin_id) {
        return coin;
      }
      return coinItem;
    });
  } else {
    coins.push(coin);
  }

  return coins;
};

const updateCoinsToShow = (coinId: string, coinIds: string[]) => {
  const coinExists: boolean =
    coinIds.filter(coinIdListItem => coinId == coinIdListItem).length > 0
      ? true
      : false;

  if (coinExists) {
    return coinIds;
  } else {
    coinIds.push(coinId);
  }
  return coinIds;
};
