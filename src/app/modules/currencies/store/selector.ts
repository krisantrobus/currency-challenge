import { createSelector } from "@ngrx/store";
import { AppStateRoot } from "src/app/store/state";
import { CoinState } from "./state";

const selectCoinFeature = (state: AppStateRoot) => state.coinFeature;

export const getStoreCoins = createSelector(
  selectCoinFeature,
  (state: CoinState) => state.coins
);

export const getSelectedCoin = createSelector(
  selectCoinFeature,
  (state: CoinState) => state.selectedCoin
);

export const getCoinsToSearch = createSelector(
  selectCoinFeature,
  (state: CoinState) => state.coinsToSearch
);

export const getShowCurrency = createSelector(
  selectCoinFeature,
  (state: CoinState) => state.showCurrency
);

export const getCurrencyList = createSelector(
  selectCoinFeature,
  (state: CoinState) => state.listOfCurrencies
);
