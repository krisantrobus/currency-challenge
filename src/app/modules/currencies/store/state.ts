import { Coin } from "src/app/interfaces/types";
import { COINS } from "src/app/interfaces/const";
import { Observable, of } from "rxjs";

const COINS_OFFLINE: Coin[] = [
  {
    coin_id: "BCH",
    coin_name: "Bitcoin-Cash",
    currency: "USD",
    currency_name: "United States Dollar",
    last_price: "220.48798547",
    price_24hr_pcnt: 4.5,
    source: "BraveNewCoin",
    success: true,
    time_stamp: "1572003961",
    utc_date: new Date("2019-10-25 11:46:01"),
    vol_24hr_pcnt: -17.18,
    volume_24hr: "543813328"
  },
  {
    coin_id: "LTC",
    coin_name: "Litecoin",
    currency: "USD",
    currency_name: "United States Dollar",
    last_price: "50.8656569",
    price_24hr_pcnt: 2.45,
    source: "BraveNewCoin",
    success: true,
    time_stamp: "1572003961",
    utc_date: new Date("2019-10-25 11:46:01"),
    vol_24hr_pcnt: -35.35,
    volume_24hr: "795501800"
  },
  {
    coin_id: "BTC",
    coin_name: "Bitcoin",
    currency: "USD",
    currency_name: "United States Dollar",
    last_price: "7601.66746363",
    price_24hr_pcnt: 2.2,
    source: "BraveNewCoin",
    success: true,
    time_stamp: "1572003961",
    utc_date: new Date("2019-10-25 11:46:01"),
    vol_24hr_pcnt: -32.6,
    volume_24hr: "4279399106"
  },
  {
    coin_id: "USDT",
    coin_name: "Tether",
    currency: "USD",
    currency_name: "United States Dollar",
    last_price: "1.01359137",
    price_24hr_pcnt: 0.33,
    source: "BraveNewCoin",
    success: true,
    time_stamp: "1572003961",
    utc_date: new Date("2019-10-25 11:46:01"),
    vol_24hr_pcnt: -42.81,
    volume_24hr: "26241411"
  }
];

export interface CoinState {
  coins: Coin[];
  selectedCoin: Coin;
  coinsToSearch: string[];
  showCurrency: string;
  listOfCurrencies: Object[];
}

export const initialCoinState: CoinState = {
  coins: [],
  selectedCoin: null,
  coinsToSearch: [
    COINS.tether,
    COINS.bitcoin,
    COINS.bitcoinCash,
    COINS.litecoin
  ],
  showCurrency: "usd",
  listOfCurrencies: []
};
