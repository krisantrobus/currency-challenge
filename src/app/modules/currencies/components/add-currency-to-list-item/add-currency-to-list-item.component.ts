import { Component, OnInit, Input } from "@angular/core";
import { CurrencyDataService } from "../../currencies.service";
import { Store } from "@ngrx/store";
import { AppStateRoot } from "src/app/store/state";
import { CoinActions } from "../../store/action.type";

@Component({
  selector: "app-add-currency-to-list-item",
  templateUrl: "./add-currency-to-list-item.component.html",
  styleUrls: ["./add-currency-to-list-item.component.scss"]
})
export class AddCurrencyToListItemComponent implements OnInit {
  @Input() private currency: Object;

  public coinId: string;
  public coinName: string;

  constructor(
    private cds: CurrencyDataService,
    private _store: Store<AppStateRoot>
  ) {}

  ngOnInit() {
    if (this.currency) {
      this.coinId = Object.keys(this.currency)[0];
      this.coinName = this.currency[this.coinId];
    }
  }

  public addCoin = () => {
    this._store.dispatch({
      type: CoinActions.UPDATE_COINS_TO_SHOW,
      payload: this.coinId
    });
    this.cds.addCoinById(this.coinId);
  };
}
