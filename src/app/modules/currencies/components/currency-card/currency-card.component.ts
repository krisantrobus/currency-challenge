import { Component, OnInit, Input } from "@angular/core";
import { Coin } from "src/app/interfaces/types";
import { Store } from "@ngrx/store";
import { CoinActions } from "../../store/action.type";
import { AppStateRoot } from "src/app/store/state";
import { Router } from "@angular/router";

@Component({
  selector: "app-currency-card",
  templateUrl: "./currency-card.component.html",
  styleUrls: ["./currency-card.component.scss"]
})
export class CurrencyCardComponent implements OnInit {
  @Input() public coin: Coin = null;
  constructor(private _store: Store<AppStateRoot>, private router: Router) {}

  ngOnInit() {}

  public getFormattedPrice = () => {
    return parseFloat(this.coin.last_price).toFixed(2);
  };

  public selectCoin = async () => {
    await this._store.dispatch({
      type: CoinActions.SELECT_COIN,
      payload: this.coin
    });

    this.router.navigate([`/currencies/detail/${this.coin.coin_id}`]);
  };
}
