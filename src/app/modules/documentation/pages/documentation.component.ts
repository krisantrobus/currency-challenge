import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-documentation",
  template: `
    <div
      id="mdFiles"
      class="w-100 h-100 p-2 m-0 scroll"
      markdown
      [src]="'/assets/README.md'"
    ></div>
  `
})
export class DocumentationComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
