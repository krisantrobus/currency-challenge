# Currency Tech Challenge

## Running the App

This is a standard angular app with no extra dependencies to build angular libraries etc. It sues Node Package Manager for dependency management, just run `npm i && ng serve`

## What's Included

- Services
  - currencies.service.ts
  - translate.service.ts
- Lazy Loading
- Inline Template, DocumentionComponent
- Inputs CurrencyCardComponent
- Directives
  - ngIf
  - ngFor
  - Custom directive - hover _see below_
- Reusable Components & Directives
  - By having its own module and exporting the component to other modules
- Observables & Subscription
- Async unsubscritpion handling
- Animations _see below_
- NgRx state management
  - Both as features and root
  - selectors
  - reducers
- Typescript interfaces used throughout

## What's not Included

- Custom Angular library. I have previously done this, complete with including multiple dependencies to ship across applications.
- E2E testing

## Key Decisions

### State Management

An application of this size does not require the use of complex state management but it has been included as a demonstration of capabilities.

State management has been used in the application. It follows the Redux pattern, implemented by NgRx. This was implemented as state management is crucial in scaling sing page applications. By using a state management library it allows new states and component states to be easily integrated and controlled.

This library provides a way to store state into one single, local, state container. It also manages the state of the data in a one way data flow

### Component Driven

As each weather object has the same data structure a stateless card component has been created. This is reused on the /weather/all home page to render each weather card. Integration is with the `@input()` and `@Output` decorator. Also utilising the `ngIf` directive

While the output captures and emits a click function it is just as easy to achieve this with the click of the `<weather-card>` DOM. I have not done this to demonstrate the ability to output from components.

```
<app-currency-card
    *ngFor="let coin of coins | async | sortBy: 'coin_name'"
    [coin]="coin"
    (click)="selectCoin(coin)"
></app-currency-card>
```

### Typscript

Typscript has been heavily used to ensure that the application is strongly typed. This gives many advantages such as utilising private and public variables and methods and defining interfaces.

### Bootstrap

Bootstrap was implemented as a simple way to style components and control the layout of the application. Examples include using the flex css features to correctly size and arrange content. It's also used to control the colours and theme the application.

Some commonly used classes:

- d-flex
- h- , w- , p- , m-
- bg-\${color}
- align-items
- justify-content

The use of media queries have also been included to create a highly responsive app fit for both web and mobile. I would encourage you to test both usecases.

## Currencies Service

The data service uses the HttpClient provided by Angular. I return Observables back to component and subscribe to retrieve the values.

The subscriptions are closed once the request has completed. The data on success is then dispatched to the store. This means the components that call the functions do not need to do any management of subscriptions. The data is reolved through the store.

I would usually on error use another service to provide feedback to the user but I've left that out of scope for this app.

the getAllSpecifiedCurrencies function uses the store values to determine which currency to search by and the loops through a list of strings (coin ids) to call the api. THese subscriptions are created when called by the AllCurrencies component and are unsubscripted once this component is destroyed

```
public getAllSpecifiedCurrencies = async () => {
    this.currencyToSearch = this._store
      .pipe(select(getShowCurrency))
      .subscribe(currency => {
        this._currencyToShow = currency;
      });

    this.coinsToSearchSubscription = this._store
      .pipe(select(getCoinsToSearch))
      .subscribe(async (coinIds: string[]) => {
        coinIds.forEach(async coinId => {
          const request: Subscription = await this.http
            .get<Coin[]>(
              `${environment.apiUrl}/ticker?show=${this._currencyToShow}&coin=${coinId}`,
              {
                headers: this._headers
              }
            )
            .subscribe(
              response => {
                this._store.dispatch({
                  type: CoinActions.UPDATE_COINS,
                  payload: response
                });
                request.unsubscribe();
              },
              err => {
                console.log(`for coin: ${coinId}`, err);
                request.unsubscribe();
              }
            );
        });
      });
  };
```

There is another method to retrieve by id which is used by the detail component.

## NgRx

NgRx has been implemented as the state management for this application. IT acts as the single truth for all state for the application.

### What's been Used:

- Store
- Reducers
- Custom Selectors

### What's not been used:

- Effects
- Actions

Usually I would use actions to handle any asynchronous calls to any APIs but in this scenario I opted for a data service to handle all of this and handle the result in the component on success. I've decided to do it this way to demonstrate knowledge of Angular Services.

### The State

The state of the application has been strongly typed with use of interfaces and initial states. Below are the snippets of code used to define this:

```

export interface AppStateRoot {
  coinFeature: CoinState;
  appState: AppState;
}

export interface AppState {
  loading: boolean;
}

export const initialAppState: AppState = {
  loading: false
};

```

```
export interface CoinState {
  coins: Coin[];
  selectedCoin: Coin;
  coinsToSearch: string[];
  showCurrency: string;
}

export const initialCoinState: CoinState = {
  coins: [],
  selectedCoin: null,
  coinsToSearch: [
    COINS.tether,
    COINS.bitcoin,
    COINS.bitcoinCash,
    COINS.litecoin
  ],
  showCurrency: "usd"
};
```

All currency components use a specific `coinReducer` which handles all interaction with the `coinFeature` segment/feature. The same is applied for the appState. This is achieved by defining the following in the modules of a component:

```
@NgModule({
    imports: [
        ...
        StoreModule.forFeatur("coinFeature", coinReducer)
    ],
})
```

### Reducers

The root reducer was created to handle to root of the state and would handle any operational state such as loading. The use of the root reducer wasn't implenented in the scope but a `coinReducer` has been cerated to handle all currency data

```
export function coinReducer(
  state: CoinState = initialCoinState,
  action: Action
) {
  switch (action.type) {
    case CoinActions.SELECT_COIN:
      return { ...state, selectedCoin: action.payload };
    case CoinActions.UPDATE_COINS:
      return {
        ...state,
        coins: updateCoin(action.payload, state.coins)
      };
    default: {
      return state;
    }
  }
}
```

A design decision that was made here was to create another funciton which would update the coins array in the store. This was implemented as each response only retrives one coin and not a list therefore requireing a merge into existing data.

```
const updateCoin = (coin: Coin, coins: Coin[]) => {
  const coinExists: boolean =
    coins.filter(coinItem => coin.coin_id == coinItem.coin_id).length > 0
      ? true
      : false;

  if (coinExists) {
    coins.map(coinItem => {
      if (coinItem.coin_id == coin.coin_id) {
        return coin;
      }
      return coinItem;
    });
  } else {
    coins.push(coin);
  }
  return coins;
};
```

### Selectors

Custom selectors have been defined for making it easier for components to subscribe to specific parts of state. This also allows for consistency across all data retrieved by components as any data manipulation has already occurred.

These selectors do not handle any complex transformation logic instead they just retrieve what is currently in the store for sepcific fields.

```
const selectCoinFeature = (state: AppStateRoot) => state.coinFeature;

export const getStoreCoins = createSelector(
  selectCoinFeature,
  (state: CoinState) => state.coins
);

export const getSelectedCoin = createSelector(
  selectCoinFeature,
  (state: CoinState) => state.selectedCoin
);

export const getCoinsToSearch = createSelector(
  selectCoinFeature,
  (state: CoinState) => state.coinsToSearch
);

export const getShowCurrency = createSelector(
  selectCoinFeature,
  (state: CoinState) => state.showCurrency
);

```

### Use in Components

The selectors are used in the components to subscribe to the state. All subscriptions are unsubscribed on destroy of the component to avoid memory leaks in the application. This is handled by using the async angular pipe in the AllCurrenciesComponent template.

```
public coins: Observable<Coin[]>;

ngOnInit() {
    // Uses async pipe to unsubscribe
    this.coins = this._store.pipe(select(getStoreCoins));
}
```

Then in the template:

```
<app-currency-card
    *ngFor="let coin of coins | async | sortBy: 'coin_name'"
    [coin]="coin"
></app-currency-card>
```

### Strongly Typed

I've ensured that all dispatches are strongly typed by using an enumerator to define all types that can occur. These are then checked in the reducers:

```
export enum CoinActions {
  SELECT_COIN = "SELECT_COIN",
  UPDATE_COINS = "UPDATE_COINS"
}
```

## Limiting API calls where applicable

I have limited the calls to the api when navigating routes to teh details page. I have achieved this by first checking whether the selectedCoin field in the api has been populated with data. Then I check to see that the id of the coin matches the router parameter. If these do not match, or it is not populated then a call is made to the api via teh currency service to retrieve and populate it.

This decision was made as the data is avaiable when navigating from the all currencies page. It is also likely to not be updated as the source data is not updated frequently enough to have a benefit to retrieve so often. It does however need to check whether this selected object is populated as on page refresh any data in the state that isn't set as default is cleared.

```
constructor(
    private _store: Store<AppStateRoot>,
    private _location: Location,
    private _activatedRoute: ActivatedRoute,
    private cds: CurrencyDataService
) {}

ngOnInit() {
    this.coin = this._store.pipe(select(getSelectedCoin));
    // Only make a call to the api if data is not selected
    // This only happens on refresh of a page
    !this.checkSelectedCoinPopulated() && this.getCoinData();
}

private checkSelectedCoinPopulated = () => {
    const coinIdToCheck: string = this.getCoinIdRouterParameter();
    let coinPopulated: boolean = false;
    this.coin.forEach((coin: Coin) => {
      coinPopulated = coin && coin.coin_id == coinIdToCheck ? true : false;
    });
    return coinPopulated;
};

private getCoinIdRouterParameter = () => {
    return this._activatedRoute.snapshot.params["coinId"];
};

private getCoinData = () => {
    this.cds.getCoinById(this.getCoinIdRouterParameter());
};
```

## Custom Directive - cardHover

A custom directive had been created to allow for hover styling on the currency cards view button. It can be applied to any element to apply a class to the element which is defined in the styles.scss to add a shadow. It has its own module, exporting itself to make it available across the whole application.

The directive acheives this by using host listener decorators to apply and remove styling using a highligh method

```
// run when hovering over the element
@HostListener("mouseenter") onMouseEnter() {
    this.highlight(true);
}

// run when leaving the element
@HostListener("mouseleave") onMouseLeave() {
    this.highlight(false);
}

// Highlighted based on whether the mouse is over the element by applying styling
private highlight(active: boolean): void {
    active
      ? this.renderer.addClass(this.el.nativeElement, "hover-directive")
      : this.renderer.removeClass(this.el.nativeElement, "hover-directive");
}
```

It's then added to the currency card component button:

```
<button class="btn btn-gradient-primary btn-block" (click)="selectCoin()" hover>
    View
</button>
```

## Animations

Angular animations have been used on the sidebar. There are 2 animations. The first is to control the movement of teh sidebar expanding and colapsing. It works by changing the margin-left property of the element to move the element out of view.

The second is to style a fade in and face out of the overlay element. The overlay is designed to only appear on smaller devices as the sidebar will then use position absolute to overlap teh page content. The overlay is clicked to collapse the sidebar again.

First both of the animation triggers are defines in the navbar.component.ts file.

```
backbase-code-challenge\src\app\modules\weather-module\components\city-modal\city-modal.component.html

<div class="card container-fluid" id="modal-container" [@openClose]="this.active ? 'open' : 'closed'">
```

Next I've defined the animation in the `@Component` decorator:

```
animations: [
    trigger("openClose", [
        state(
            "open",
            style({
            marginLeft: "0"
            })
        ),
        state(
            "closed",
            style({
            marginLeft: "-170px"
            })
        ),
        transition("open => *", [animate("0.3s 200ms ease-out")]),
        transition("closed => open", [animate("0.3s 200ms ease-in")])
    ]),
    trigger("overlay", [
        state(
            "show",
            style({
            opacity: 0.2
            })
        ),
        state(
            "hide",
            style({
            opacity: 0,
            display: "none"
            })
        ),
        transition("show => *", [animate("0.3s 200ms ease-out")]),
        transition("hide => show", [animate("0.3s 200ms ease-in")])
    ])
]
```

The trigger is then registered on the element to apply it.

```
<nav
  class="d-flex flex-column h-100 bg-primary-light"
  [@openClose]="this.active ? 'open' : 'closed'"
>
    ...
</nav>

<div
    id="overlay"
    [@overlay]="this.active ? 'show' : 'hide'"
    (click)="toggleExpand()"
></div>
```

## Testing

Integration testing has been done on one of the components: DetailCurrencyComponent.

This component has been selected as it shows knowledge of writting tests for components that use router parameters, service calls and NgRX stores.

It tests logic for:

- all scenarios of state population and matching id wiht routers
- Element renders correctly when passing in an object
- Methods to calculate prices are called
